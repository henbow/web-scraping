import hashlib, time, datetime, urlparse

'''
Created on Aug 7, 2014

@author: hendro
'''

def generate_hash(string):
    md5hash = hashlib.md5()
    md5hash.update(string)

    return md5hash.hexdigest()

def convert_unix_timestamp_to_datetime(unix_timestamp):
    return datetime.datetime.fromtimestamp(float(unix_timestamp))

def convert_datetime_to_unix_timestamp(date_string, format="%Y/%m/%d %H:%M"):
    try:
        return int(time.mktime(datetime.datetime.strptime(date_string[:16], format).timetuple()))
    except ValueError:
        return 0

def clean_url(url, keep_params=('ID=',)):
    parsed = urlparse.urlsplit(url)
    filtered_query = '&'.join(qry_item for qry_item in parsed.query.split('&') if qry_item.startswith(keep_params))

    return urlparse.urlunsplit(parsed[:3] + (filtered_query,) + parsed[4:])

def parse_config(filename):
    config = {}
    f = open(filename, 'r')

    for line in f.readlines():
        if line[0] != '#':
            config_per_line = line.split(': ')
            if len(config_per_line) > 1:
                config[config_per_line[0]] = config_per_line[1].rstrip()
    f.close()

    return config

def performance_bonus_type(img_url):
    pb_type = urlparse.urlparse(img_url).path.split('/')
    return pb_type[-1].split('.')[0]

def type_parser(url):
    path = urlparse.urlparse(url).path.split('/')
    return path[1]